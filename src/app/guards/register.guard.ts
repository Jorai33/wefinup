import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SettingsService } from '../services/settings.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterGuard implements CanActivate {

  constructor( private settingsServ : SettingsService, private router : Router){}
  
  
  canActivate(): boolean {
      if(this.settingsServ.getSetttings().allowRegistration){
        return true;
      }else{
        // if registration not allowed it sends us to the login page
        this.router.navigate(['/login']);
        return false;
      }
    }

   
}

