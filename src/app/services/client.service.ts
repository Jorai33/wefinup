import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Client } from '../models/Client';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientsCollection: AngularFirestoreCollection<Client>;
  clientDoc: AngularFirestoreDocument<Client>;
  clients: Observable<Client[]>;
  client: Observable<Client>;

  constructor(private angularFS: AngularFirestore) {
    this.clientsCollection = angularFS.collection('Clients', ref =>
      ref.orderBy("solde", "asc")
    ); // trie les clients par leur solde
  }

  getClients(): Observable<Client[]> {
    this.clients = this.clientsCollection.snapshotChanges().pipe(
      map(changes => changes.map(action => {
        const data = action.payload.doc.data() as Client;
        const id = action.payload.doc.id;
        return { id, ...data };
      }))
    );

    return this.clients;
  }

  addClient(client: Client) {
    this.clientsCollection.add(client);
  }

  getClient(id: string): Observable<Client> {
    this.clientDoc = this.angularFS.doc<Client>(`Clients/${id}`);
   
    this.client = this.clientDoc.snapshotChanges().pipe(
      map(action => {
        if(action.payload.exists === false) {
          return null;
        } else {
          const data = action.payload.data() as Client;
          const id = action.payload.id;
          return { id, ...data };
        }
    })); 
 
    return this.client;
  }


updateClient(client : Client){
  this.clientDoc = this.angularFS.doc(`Clients/${client.id}`);
  this.clientDoc.update(client);
}



deleteClient(client : Client){
  this.clientDoc = this.angularFS.doc(`Clients/${client.id}`);
  this.clientDoc.delete();
}



}





