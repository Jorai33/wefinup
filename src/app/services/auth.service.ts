import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { LocalStorage } from '@ngx-pwa/local-storage';
//import { auth } from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn: boolean;
  redirectUrl: string;
  //userData: any;

  constructor(private afAuth: AngularFireAuth, private localStorage: LocalStorage) {

/* Saving user data as an object in localstorage if logged out than set to null */
// this.afAuth.authState.subscribe(user => {
//   if (user) {
//   this.userData = user; // Setting up user data in userData var
//   localStorage.setItem('user', JSON.stringify(this.userData));
//   JSON.parse(localStorage.getItem('user'));
//   } else {
//   localStorage.setItem('user', null);
//   JSON.parse(localStorage.getItem('user'));
//   }
//   })

  }

  seConnecter(email: string, motDePasse: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, motDePasse).then(userData => resolve(userData),
        erreur => reject(erreur));
      this.isLoggedIn = true;
    });
  }

  verifierConnection() {
    return this.afAuth.authState.pipe(action => action);
  }

  seDeconnecter() {
    this.afAuth.auth.signOut();
    this.isLoggedIn = false;
  }

  inscrire(email: string, motDePasse: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, motDePasse).then(userData => resolve(userData),
        erreur => reject(erreur));
      this.isLoggedIn = true;
    });
  }


}
