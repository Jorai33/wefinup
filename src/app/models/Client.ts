export interface Client {
  id? : string,
  prenom? : string,
  nom? : string,
  email? : string,
  telephone? : string,
  solde? : number
}