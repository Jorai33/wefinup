import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/Client';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages'
import { SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  id: string;
  client : Client = {
    prenom:"",
    nom:"",
    email:"",
    telephone:"",
    solde : 0
  }

  disableBalanceOnEdit : boolean;


  constructor(private clientServ: ClientService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessage: FlashMessagesService,
    private settingsServ : SettingsService) { }

    ngOnInit() {
      this.disableBalanceOnEdit = this.settingsServ.getSetttings().disableBalanceOnEdit;
      // Get id from url
      this.id = this.route.snapshot.params['id'];   
      //Get client with that id  
      this.clientServ.getClient(this.id).subscribe(client => this.client = client );  
        
    }

    onSubmit({value, valid} : {value:Client , valid : boolean}){
      if(!valid){
        this.flashMessage.show("Svp remplissez le formulaire correctement", {cssClass : 'alert-danger', timeout : 4000});
      }else{
        //add id to client
        value.id = this.id;
        // update client
        this.clientServ.updateClient(value);
        this.flashMessage.show( "Client modifié", {cssClass : 'alert-danger', timeout : 4000});
        this.router.navigate(["/client/"+this.id]);
      }

    }

}
