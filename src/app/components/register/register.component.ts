import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email : string;
  motDePasse : string;

  constructor( 
    private authServ : AuthService,
    private flashMessage : FlashMessagesService,
    private router : Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.authServ.inscrire(this.email, this.motDePasse).then( result => {
      
      this.flashMessage.show("Vous êtes désormais inscrit et connecté", {cssClass : 'alert-success', timeout : 4000 });
      this.router.navigate(['/']);
    }).catch(err => {
      this.flashMessage.show(err.message, {cssClass : 'alert-danger', timeout : 4000 });
    });
  
  
  }

}
