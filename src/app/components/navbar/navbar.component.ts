
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages'
import { AuthService } from 'src/app/services/auth.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  estConnecte : boolean;
  clientConnecte : string;
  afficherRegister : boolean ;


  constructor(
    private authServ : AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private settingsServ : SettingsService) { }

  ngOnInit() {
    this.authServ.verifierConnection().subscribe(authentifie => {
      if (authentifie){
        this.estConnecte = true;
        this.clientConnecte = authentifie.email;
      }else{
        this.estConnecte = false;
      }
    });
    this.afficherRegister = this.settingsServ.getSetttings().allowRegistration;
  }

  onLogoutClick(){
    this.authServ.seDeconnecter();
    this.authServ.isLoggedIn = false;
    this.flashMessage.show("Vous êtes maintenant déconnecté", {cssClass: 'alert-success', timeout : 4000});
    this.router.navigate(['/login']);
  }

}
