import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages'
import { SettingsService } from '../../services/settings.service';
import { Settings } from '../../models/Settings';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  settings : Settings;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private settingsServ : SettingsService
  ) { }

  ngOnInit() {
    this.settings = this.settingsServ.getSetttings();
  }

  onSubmit(){
    this.settingsServ.changeSettings(this.settings);
    this.flashMessage.show("Setting saved", {cssClass : 'alert-success' ,timeout : 4000});
  }

}
