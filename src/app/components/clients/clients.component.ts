import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/Client';


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients: Client[];
  detteTotale: number;

  constructor(private clientServ: ClientService) { }

  ngOnInit() {
    this.clientServ.getClients().subscribe(clients => {
      //console.log(clients); affiche clients firebase dans la console
      this.clients = clients; // affecte les clients Firebase au tableau de Client[] Angular
      this.getDetteTotale();
    });
  }

  getDetteTotale() {
    this.detteTotale = this.clients.reduce((total, client) => {
      return total + parseFloat(client.solde.toString());
    }, 0);
  }
}
