import { Component, OnInit, ViewChild } from '@angular/core';
import { Client } from '../../models/Client';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ClientService } from '../../services/client.service';
import { Router } from '@angular/router';
import { SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  @ViewChild("formulaireInscription") form : any;
  
  client : Client = {
    prenom : "",
    nom : "",
    email:"",
    telephone :"",
    solde : 0
  };

  disableBalanceOnAdd : boolean;

  constructor(  private flashMessage : FlashMessagesService, 
                private clientServ : ClientService,
                private router : Router,
                private settingsServ : SettingsService
    ) { }

  ngOnInit() {
    this.disableBalanceOnAdd = this.settingsServ.getSetttings().disableBalanceOnAdd;
  }


  onSubmit({value, valid} : {value : Client, valid : boolean}){
    
    if(this.disableBalanceOnAdd){
      value.solde = 0;
    }
    
    if(!valid){
      //show error
      this.flashMessage.show("Vous avez des erreurs", {cssClass : "alert-danger", timeout : 4000});
    }else{
      // add new client 
      this.clientServ.addClient(value);
      // Show Message
      this.flashMessage.show("Nouveau Client Ajouté", {cssClass : "alert-success", timeout : 4000});
      // Go to dashboard
      this.router.navigate(['/']);

    }
  }

}
