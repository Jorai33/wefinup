import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/Client';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages'

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {
  id: string;
  client: Client;
  soldeExistant: boolean = false;
  afficherInputModifSolde: boolean = false;


  constructor(
    private clientServ: ClientService,
    private router: Router,
    private route: ActivatedRoute,
    private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    // Get id from url
    this.id = this.route.snapshot.params['id'];   
    //Get client with that id  
    this.clientServ.getClient(this.id).subscribe(client => {
      if(client != null){
        if(client.solde > 0){
          this.soldeExistant = true;
        }
      }      
      this.client = client;
      console.log(this.client);        
      });
  }

  modifierSolde(){
    this.clientServ.updateClient(this.client);
    this.flashMessage.show("Solde modifié", { 
      cssClass: 'alert-success', timeout : 4000
    });
  }

  onDeleteClick(){
    if(confirm("Etes-vous sûr ?")){
      this.clientServ.deleteClient(this.client);
      this.flashMessage.show("Client supprimé", { 
        cssClass: 'alert-success', timeout : 4000
      });
      this.router.navigate(['/']);

    }
  }

}
