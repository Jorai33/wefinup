import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email : string;
  motDePasse : string;

  constructor(
    private authServ : AuthService,
    private router: Router,
    private flashMessage : FlashMessagesService
  ) { }

  ngOnInit() {
    // if user is registered go to dashboard
    
    this.authServ.verifierConnection().subscribe(authentification => {
      if (authentification){
        this.router.navigate(['/']);
      }
    })
   
  }

  onSubmit(){
    this.authServ.seConnecter(this.email,this.motDePasse).then(reponse => {
      this.flashMessage.show("Vous êtes connecté", {cssClass : 'alert-success', timeout : 4000});
      this.authServ.isLoggedIn = true;
      this.router.navigate(['/']);
    }).catch(erreur => {
      this.flashMessage.show(erreur.message, {cssClass : 'alert-danger', timeout : 4000});
      this.authServ.isLoggedIn = false;
    });
    
  }

}
