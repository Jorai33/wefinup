// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyALS3jd4OcFF2iJ5YNU6hgV3gL2jM0E_Ao",
    authDomain: "client-panel-prod-3b86a.firebaseapp.com",
    databaseURL: "https://client-panel-prod-3b86a.firebaseio.com",
    projectId: "client-panel-prod-3b86a",
    storageBucket: "client-panel-prod-3b86a.appspot.com",
    messagingSenderId: "254312877235",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
